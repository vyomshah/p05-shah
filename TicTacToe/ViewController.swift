//
//  ViewController.swift
//  TicTacToe
//
//  Created by Jenil Shah on 4/2/17.
//  Copyright © 2017 NerdAttack. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var count = 1     //0 means x and 1 means o
    
    var gamestate = [0,0,0,0,0,0,0,0,0] //Initially every button set to 0
    
    let winner = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]   //Mention all the winning combination which is array of an array
   var active = true
    
    
    @IBOutlet var button: UIButton!

    @IBOutlet var gameover: UILabel!
    
    @IBAction func buttonclicked(_ sender: Any) {
        
        if(gamestate[(sender as AnyObject).tag] == 0 && active){    //won't let the x and o overlap
            
            gamestate[(sender as AnyObject).tag] = count

        
            if count == 1{
                (sender as AnyObject).setImage(UIImage(named: "x.png"), for: .normal)     //On clicking in the beginning it will change to x
                count = 2
                
            }
            else {
                (sender as AnyObject).setImage(UIImage(named: "o.png"), for: .normal) //will change to o on the second click
                count = 1
            }
            
            for combination in winner{
                
                if(gamestate[combination[0]] != 0 && gamestate[combination[0]] == gamestate[combination[1]] && gamestate[combination[1]] == gamestate[combination[2]]){
                    
                    active = false
                    
                    if(gamestate[combination[0]] == 1){
                        gameover.text = "Player X wins the game!!"
                      //  print("X won the game")
                    }
                    else if(gamestate[combination[0]] == 2){
                        gameover.text = "Player O wins the game!!"
                    // print("O won the game")
                    }
                    
                    else{
                        gameover.text = "Player AB wins the game!!"
                    }
                    
                    gameover.isHidden = false
                }
                
            }
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // test.setImage(UIImage(named: "oo.png"), for: .normal)
        
        gameover.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

